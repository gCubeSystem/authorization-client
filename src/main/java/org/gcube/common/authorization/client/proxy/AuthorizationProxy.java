package org.gcube.common.authorization.client.proxy;

import java.util.List;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.endpoints.AuthorizationEndpoint;
import org.gcube.common.authorization.library.endpoints.EndpointsContainer;
import org.gcube.common.authorization.library.provider.UserInfo;

public interface AuthorizationProxy {

	AuthorizationEndpoint getEndpoint(int infrastructureHash);
	
	void setEndpoint(EndpointsContainer endpoints);
		
	AuthorizationEntry get(String token) throws ObjectNotFound, Exception;
	
	List<AuthorizationEntry> get(List<String> tokens) throws ObjectNotFound, Exception;

	String generateUserToken(UserInfo client, String context)
			throws Exception;

	String resolveTokenByUserAndContext(String user, String context)
			throws Exception;

	void removeAllReleatedToken(String clientId, String context) throws Exception;

	void setTokenRoles(String token, List<String> roles) throws Exception;
	
}
