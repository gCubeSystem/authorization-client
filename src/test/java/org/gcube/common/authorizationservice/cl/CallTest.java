package org.gcube.common.authorizationservice.cl;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.util.ArrayList;

import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.junit.Test;

public class CallTest {
	
	@Test
	public void haspred4s() {
		System.out.println("pred4s".hashCode() & 0xfffffff);
	}
	
	@Test
	public void getToken()  throws Exception {
		System.out.println(authorizationService().get("XXXXXXXX"));
		//System.out.println(authorizationService().get("XXXXXXXX").getContext());
		//System.out.println(authorizationService().get("XXXXXXXX").getMap());
	}
	
	@Test
	public void removeUserinContext()  throws Exception {
		authorizationService().removeAllReleatedToken("lucio.lelii", "/gcube/devsec");
	}
	

	@Test
	public void generateToken() throws Exception{
		System.out.println(authorizationService().generateUserToken(new UserInfo("guest", new ArrayList<String>()), "/pred4s"));
	}

	@Test
	public void createTestToken()  throws Exception {
		System.out.println(requestTestToken("/pred4s"));
	}
	
	@Test
	public void resoveTokenAndPrint() throws Exception{
		System.out.println(resolveToken("fa7ed90a-d9d6-4bfb-95c9-5e3d7b6cd23e-843339462"));
	}

	private String requestTestToken(String context) throws Exception{
		return authorizationService().generateUserToken(new UserInfo("lucio.lelii", new ArrayList<String>()), context);
	}

	private AuthorizationEntry resolveToken(String token) throws Exception{
		AuthorizationEntry entry = authorizationService().get(token);
		return entry;
	}


}
